package com.mastertech.itau.game.questions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class GameQuestionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameQuestionServiceApplication.class, args);
    }


}
