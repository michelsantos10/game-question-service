package com.mastertech.itau.game.questions.domain.service;

import com.mastertech.itau.game.questions.domain.entity.Question;
import com.mastertech.itau.game.questions.domain.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {

    @Autowired
    public QuestionRepository questionRepository;

    public List<Question> getQuestions(String gameName) {
        return questionRepository.findByGameKey(gameName);
    }
}
