package com.mastertech.itau.game.questions.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the question database table.
 */
@Entity
@Table(name = "question")
@NamedQuery(name = "Question.findAll", query = "SELECT q FROM Question q")
public class Question implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    @Column(name = "game_key")
    private String gameKey;

    //bi-directional many-to-one association to Answer
    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    private List<Answer> answers;

    //bi-directional many-to-one association to Type
    @ManyToOne
    @JoinColumn(name = "classificacao_id")
    private Type type;

    public Question() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGameKey() {
        return this.gameKey;
    }

    public void setGameKey(String gameKey) {
        this.gameKey = gameKey;
    }

    public List<Answer> getAnswers() {
        return this.answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Answer addAnswer(Answer answer) {
        getAnswers().add(answer);
        answer.setQuestion(this);

        return answer;
    }

    public Answer removeAnswer(Answer answer) {
        getAnswers().remove(answer);
        answer.setQuestion(null);

        return answer;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}