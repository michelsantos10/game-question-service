package com.mastertech.itau.game.questions.application.message.queue;

import com.mastertech.itau.game.questions.domain.entity.Answer;
import com.mastertech.itau.game.questions.domain.entity.Question;
import com.mastertech.itau.game.questions.domain.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.JmsMessageHeaderAccessor;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class QuestionQueue {

    @Autowired
    public QuestionService questionService;

    @Autowired
    private JmsTemplate jmsTemplate;

    @JmsListener(destination = "${com.mastertech.itau.game.questions.amqp.queue.all-questions-queue}", containerFactory = "questionJmsFactory")
    public void receiveRequestAllQuestions(@Payload Map message, JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
        jmsTemplate.send(jmsMessageHeaderAccessor.getReplyTo(), new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                List<Map> questionsToSend = new ArrayList<>();
                List<Question> questions = questionService.getQuestions("conhecimento-geral");
                for (Question databaseQuestion : questions) {
                    Map<String, String> questionMap = new HashMap<>();
                    questionMap.put("title", databaseQuestion.getDescription());
                    questionMap.put("category", databaseQuestion.getType().getDescription());
                    Short i = 1;
                    for (Answer answer : databaseQuestion.getAnswers()) {
                        questionMap.put("option" + i.toString(), databaseQuestion.getAnswers().get(i - 1).getDescription());
                        if (answer.getIsCorrect() == 1) {
                            questionMap.put("answer", i.toString());
                        }
                        i++;
                    }
                    questionsToSend.add(questionMap);
                }
                Message messageTosend = session.createObjectMessage((Serializable) questionsToSend);
                messageTosend.setJMSCorrelationID(jmsMessageHeaderAccessor.getCorrelationId());
                return messageTosend;
            }
        });
    }

//    @JmsListener(destination = "c2.queue.question.id", containerFactory = "questionJmsFactory")
//    public void receiveRequestAllIdQuestions(@Payload String message, JmsMessageHeaderAccessor jmsMessageHeaderAccessor) {
//
//    }
}
