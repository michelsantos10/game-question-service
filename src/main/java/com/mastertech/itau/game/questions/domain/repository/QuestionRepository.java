package com.mastertech.itau.game.questions.domain.repository;

import com.mastertech.itau.game.questions.domain.entity.Question;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface QuestionRepository extends CrudRepository<Question, Long> {
    List<Question> findByGameKey(String gameKey);

    List<Question> findByGameKeyAndAnswersId(String gameKey, Long AnswersId);
}
