package com.mastertech.itau.game.questions.domain.repository;

import com.mastertech.itau.game.questions.domain.entity.Answer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerRepository extends CrudRepository<Answer, Long> {
    List<Answer> findByQuestionId(Long questionId);

    long countByQuestionId(Long questionId);
}
