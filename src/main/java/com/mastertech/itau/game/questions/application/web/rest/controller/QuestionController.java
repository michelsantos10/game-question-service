//package com.mastertech.itau.game.questions.application.web.rest.controller;
//
//import com.mastertech.itau.game.questions.application.message.queue.QuestionQueue;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//public class QuestionController {
//    @Autowired
//    public QuestionQueue questionQueue;
//
//    @GetMapping(path = "/api/questions/{game-name}")
//    public ResponseEntity<?> getQuestions(@PathVariable("game-name") String gameName) {
//        questionQueue.requestQuestions(gameName);
//        return ResponseEntity.ok().build();
//    }
//}
