<html><body><pre>
# game-question-service: http://142.93.61.18:9090/api


spring-data: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
procurar por: Table 3. Supported keywords inside method names


usar o link para formatar: https://jsonformatter.curiousconcept.com/

{
  "description" : "Wale's song 'Bad' featured what vocalist?",
  "gameKey" : "game1",
  "answers" : [ {
    "description" : "Rihanna",
    "isCorrect" : 0,
    "_links" : {
      "question" : {
        "href" : "http://localhost:8080/question/1"
      }
    }
  }, {
    "description" : "Tiara Thomas",
    "isCorrect" : 0,
    "_links" : {
      "question" : {
        "href" : "http://localhost:8080/question/1"
      }
    }
  }, {
    "description" : "Miguel",
    "isCorrect" : 0,
    "_links" : {
      "question" : {
        "href" : "http://localhost:8080/question/1"
      }
    }
  }, {
    "description" : "B.o.B.",
    "isCorrect" : 0,
    "_links" : {
      "question" : {
        "href" : "http://localhost:8080/question/1"
      }
    }
  } ],
  "type" : {
    "description" : "Hip Hop Music",
    "_links" : {
      "questions" : [ {
        "href" : "http://localhost:8080/question/1"
      }, {
        "href" : "http://localhost:8080/question/6"
      } ]
    }
  },
  "_links" : {
    "self" : {
      "href" : "http://localhost:8080/question/1"
    },
    "question" : {
      "href" : "http://localhost:8080/question/1"
    }
  }
}
</pre></body></html>